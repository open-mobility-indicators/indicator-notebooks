# Open Mobility Indicators – Notebooks

## Installation

```bash
pip install -r .          # install all needed packages to make notebooks calculations
pip install jupyterlab    # install Jupyter Lab, to edit Notebooks
pip install nbdime        # install nbdiff (https://github.com/tarmstrong/nbdiff) to make it easier to view Notebooks diffs (in Git especially)
```

### About nbdime configuration

Having a clear Notebooks diffs in Git is not trivial. We had to play a little with nbdiff configuration. Here's what works for us so far.
This configuration should be put in `./nbdime_config.json` file (or any file listed in the output of `jupyter --paths`).

```json
{
    "NbDiff": {
        "Ignore": {
            "/cells/*/outputs": true,
            "/cells/*/metadata": [
                "collapsed",
                "autoscroll",
                "deletable",
                "editable"
            ]
        },
       "details": false
    }
}
```

## Run Jupyter Lab

To run jupyter lab, you can use:

```bash
jupyter lab --no-browser --NotebookApp.token='' .
```

## About Notebooks organization

Each notebook is stored in a sub-directory of [`notebooks`](./notebooks) and must follow some conventions regarding its input parameters, output files and other companion files.

Those conventions allow the automatic workflow to compute all the notebooks at once.

### Notebook name

The name of the notebook should be the same as the sub-directory. For example: `./notebooks/omi_score/omi_score.ipynb`.

### Input parameters

The following parameters are common to all the notebooks. They are designed to be overloaded by [Papermill](https://papermill.readthedocs.io/en/latest/) for batch notebook execution.

- `debug` (`bool`): if `True`, the cells starting with `if debug:` will be computed
- `download_data` (`bool`): if `True`, the notebook should start by downloading the data it needs to run
- `location` (`str`): for notebooks that use the Overpass API, defines the location to process
- `output_geojson` (`str`): file path of the generated GeoJSON file

### GeoJSON feature properties

The features of the GeoJSON output file can have special properties that will be interpreted in the frontend.

- `color` (`str`): the color of the feature

### Popup template

The frontend displays a popup when clicking on a GeoJSON feature. The content of the popup can be defined besides the notebook file, in a file called `popup.template` (TODO update the name when the template engine will be chosen).

Available substitution variables:

- every GeoJSON feature property

Example:

```text
The score is {score}!
```

## Architecture

- this repo contains many notebooks
- each notebook comes with its `environment.yml` file containing frozen dependencies for [conda](https://docs.conda.io/en/latest/)

### Pipeline

To compute the notebooks on a large geo area (e.g. PACA) it is required to use a powerful machine.

The process has been modelized in a [Tekton](https://tekton.dev/) pipeline available in the [omi-ops repo](https://gitlab.com/open-mobility-indicators/omi-ops/-/tree/master/k8s/compute-notebooks).

### Limitations

This repo does not make use of container images for now.

As a consequence the environment must be created and deps installed each time the pipeline runs.

With containers, this step would be done only when deps change.
